import { createContext } from "react";

export default sidebarContext = createContext(null);
