import React, { useState, useContext } from "react";

import MotionTab from "../components/Features/Motion";
import LooksTab from "../components/Features/Looks";
import EventsTab from "../components/Features/Event";
import ControlTab from "../components/Features/Control";

import "./Styles/Sidebar.css";

export default function Sidebar() {
  const [title, setTitle] = useState("Motion");

  const [MotionFlag, setMotionFlag] = useState(true);
  const [LooksFlag, setLooksFlag] = useState(false);
  const [EventsFlag, setEventFlag] = useState(false);
  const [controlFlag, setControlFlag] = useState(false);
  const [active, setActive] = useState("Motion");

  const features = [
    {
      title: "Motion",
      bg: "#3b82f6",
    },
    {
      title: "Looks",
      bg: "#9966ff",
    },
    {
      title: "Events",
      bg: "#f59e0b",
    },
    {
      title: "Control",
      bg: "#40bf4a",
    },
  ];

  function handleTitleChange(id) {
    console.log(id);
    console.log(active);

    if (id == "Motion") {
      console.log("true");
    }

    switch (active) {
      case "Motion":
        setMotionFlag(false);
        break;
      case "Looks":
        setLooksFlag(false);
        break;
      case "Events":
        setEventFlag(false);
        break;
      case "Control":
        setControlFlag(false);
        break;
    }

    switch (id) {
      case "Motion":
        setMotionFlag(true);
        setActive("Motion");
        break;
      case "Looks":
        setLooksFlag(true);
        setActive("Looks");
        break;
      case "Events":
        setEventFlag(true);
        setActive("Events");
        break;
      case "Control":
        setControlFlag(true);
        setActive("Control");
        break;
    }

    setTitle(id);
  }

  return (
    <div className="w-60  flex flex-row items-start border-gray-200">
      {/* sidebar left content */}
      <div className="w-20 flex flex-col items-start  border-gray-200 sideBarLeft">
        {features.map((item) => {
          const { title, bg } = item;

          return (
            <div
              onClick={() => {
                handleTitleChange(title);
              }}
              className="featureContainer"
            >
              <div
                style={{ backgroundColor: bg }}
                className="featureIcon"
              ></div>
              <div>{title}</div>
            </div>
          );
        })}
      </div>

      {/* sidebar right content */}
      <div className="w-80 flex flex-col items-start border-gray-200 sideBarRight">
        <span style={{ color: "black", "font-weight": "500" }}>{title}</span>

        {MotionFlag ? <MotionTab /> : ""}
        {LooksFlag ? <LooksTab /> : ""}
        {EventsFlag ? <EventsTab /> : ""}
        {controlFlag ? <ControlTab /> : ""}
      </div>
    </div>
  );
}
