import React from "react";

export default function Control() {
  return (
    <div>
      <div className="flex flex-row flex-wrap 00 text-white px-2 py-1 my-2 text-sm cursor-pointer four">
        {"Wait"}
      </div>
      <div className="flex flex-row flex-wrap  text-white px-2 py-1 my-2 text-sm cursor-pointer four">
        {"repeat"}
        {"forever"}
      </div>
      <div className="flex flex-row flex-wrap  text-white px-2 py-1 my-2 text-sm cursor-pointer four">
        {"forever"}
      </div>
      <div className="flex flex-row flex-wrap  text-white px-2 py-1 my-2 text-sm cursor-pointer four">
        {"if/else"}
      </div>
    </div>
  );
}
