import React from "react";
import Icon from "../Icon";

export default function Event() {
  return (
    <div>
      <div className="flex flex-row flex-wrap  text-white px-2 py-1 my-2 text-sm cursor-pointer three">
        {"When "}
        <Icon name="flag" size={15} className="text-green-600 mx-2" />
        {"clicked"}
      </div>
      <div className="flex flex-row flex-wrap  text-white px-2 py-1 my-2 text-sm cursor-pointer three">
        {"When this sprite clicked"}
      </div>
    </div>
  );
}
